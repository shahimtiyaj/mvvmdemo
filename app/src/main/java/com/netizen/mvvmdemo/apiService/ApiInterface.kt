package com.netizen.netiworld.rest

import com.netizen.mvvmdemo.model.BalanceDepositGetData
import com.netizen.mvvmdemo.model.BalanceDepositPostData
import com.netizen.mvvmdemo.model.PostResponse
import retrofit2.Call
import retrofit2.http.*


interface ApiInterface {
    @FormUrlEncoded
    @POST("oauth/token")
    fun postData(@FieldMap params: HashMap<String?, String?>, @HeaderMap headers: HashMap<String?, String?>): Call<PostResponse>

    @POST("user/balance/requests/by/date_range")
    fun balanceDepositGetData(@Body obj: BalanceDepositPostData, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceDepositGetData>>
}


