package com.netizen.mvvmdemo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.netizen.mvvmdemo.data.User
import com.netizen.mvvmdemo.model.BalanceDepositGetData

@Database(entities = [User::class, BalanceDepositGetData::class], version = 8, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

//    abstract val dao: DAO
//    abstract fun dao(): DAO
//    fun dao(): DAO? = null
    abstract fun balanceDAO(): BalanceDepositDAO
    abstract fun userDao(): UserDAO

    companion object {

        @Volatile
        private var INSTANCE: LocalDatabase? = null

        @Synchronized
        fun getInstance(context: Context): LocalDatabase {
            var instance = INSTANCE

            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    LocalDatabase::class.java,
                    "local_database"
                ).fallbackToDestructiveMigration().build()

                INSTANCE = instance
            }

            return instance
        }
    }
}