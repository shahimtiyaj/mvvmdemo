package com.netizen.mvvmdemo.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.netizen.mvvmdemo.data.User

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(userList: List<User>)

    @Query("Select * from Users")
    fun getUserList(): List<User>
}