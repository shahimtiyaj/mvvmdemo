package com.netizen.mvvmdemo.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.netizen.mvvmdemo.model.BalanceDepositGetData

@Dao
interface BalanceDepositDAO {

    @Insert
    @JvmSuppressWildcards
    fun insertBalanceDeposit(balanceDepositGetData: BalanceDepositGetData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertBalanceDepositList(balanceDepositGetDataList: List<BalanceDepositGetData>)

    @Query("Select * from BalanceDeposit")
    fun getAllBalanceDepositList(): LiveData<List<BalanceDepositGetData>>

    @Query("Delete from BalanceDeposit")
    fun deleteAll()
}