package com.netizen.mvvmdemo.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.model.BalanceDepositGetData
import com.netizen.mvvmdemo.utils.MyUtilsClass

class BalanceDepositListAdapter(val context: Context,
                                val balanceDepositList: List<BalanceDepositGetData>,
                                val listener: OnItemClick) :
    RecyclerView.Adapter<BalanceDepositListAdapter.BankDepositViewHolder>(), Filterable {

    var filteredBalanceDepositList: List<BalanceDepositGetData>? = null

    interface OnItemClick {
        fun goToSingle(balanceDepositGetData: BalanceDepositGetData)
    }

    inner class BankDepositViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var request_date: TextView
        var bank_name_val: TextView
        var bank_branch_val: TextView
        var ac_number_val: TextView
        var request_amt_val: TextView
        var trans_date_val: TextView
        var trans_id_val: TextView
        var processed_date_val: TextView
        var status_val: TextView

        init {
            request_date= view.findViewById(R.id.request_date) as TextView
            bank_name_val = view.findViewById(R.id.bank_name_val) as TextView
            bank_branch_val = view.findViewById(R.id.bank_branch_val) as TextView
            ac_number_val = view.findViewById(R.id.ac_number_val) as TextView
            request_amt_val = view.findViewById(R.id.request_amt_val) as TextView
            trans_date_val = view.findViewById(R.id.trans_date_val) as TextView
            trans_id_val = view.findViewById(R.id.trans_id_val) as TextView
            processed_date_val = view.findViewById(R.id.processed_date_val) as TextView
            status_val = view.findViewById(R.id.status_val) as TextView
        }
    }

    init {
        this.filteredBalanceDepositList = balanceDepositList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankDepositViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.logs_wallet_deposit_row, parent, false)
        // Return a new holder instance
        return BankDepositViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: BankDepositViewHolder, position: Int) {
        // Get the item model based on position
        val balanceDeposit = filteredBalanceDepositList?.get(position)

        holder.request_date.text = balanceDeposit?.requestDate?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.bank_name_val.text = balanceDeposit?.bank
        holder.bank_branch_val.text = balanceDeposit?.fromWhere
        holder.ac_number_val.text = balanceDeposit?.accountNumber
        holder.request_amt_val.text =String.format("%,.2f",balanceDeposit?.requestedAmount)
        holder.trans_date_val.text = balanceDeposit?.transactionDate?.toLong()?.let { MyUtilsClass.getDate(it) }
        holder.trans_id_val.text = balanceDeposit?.transactionNumber
        holder.processed_date_val.text = balanceDeposit?.approveDate?.toLong()?.let { MyUtilsClass.getDate(it) }

        when (balanceDeposit?.requestStatus) {
            "Approved" -> {
                holder.status_val.text = balanceDeposit.requestStatus
                holder.status_val.setTextColor(Color.parseColor("#44bd32"))
            }
            "Rejected" -> {
                holder.status_val.text = balanceDeposit.requestStatus
                holder.status_val.setTextColor(Color.parseColor("#e84118"))
            }
            else -> {
                holder.status_val.text = balanceDeposit?.requestStatus
                holder.status_val.setTextColor(Color.parseColor("#0097e6"))
            }
        }

        holder.itemView.setOnClickListener {
            listener.goToSingle(balanceDeposit!!)
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredBalanceDepositList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredBalanceDepositList = balanceDepositList
                } else {
                    val filteredList = ArrayList<BalanceDepositGetData>()
                    for (row in balanceDepositList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.bank!!.toLowerCase().contains(charString.toLowerCase()) || row.bank!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredBalanceDepositList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredBalanceDepositList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredBalanceDepositList = filterResults.values as List<BalanceDepositGetData>?
                notifyDataSetChanged()
            }
        }
    }
}