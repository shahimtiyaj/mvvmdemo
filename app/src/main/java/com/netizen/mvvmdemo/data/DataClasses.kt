package com.netizen.mvvmdemo.data

import androidx.room.Entity
import androidx.room.PrimaryKey

//@Entity(tableName = "BalanceDeposit")
data class BalanceDeposit (

    var requestDate: String? = null,

    var bank: String? = null,

    var fromWhere: String? = null,

    var accountNumber: String? = null

//    var requestedAmount: Double? = null,
//
//    var transactionDate: String? = null,
//
//    var transactionNumber: String? = null,
//
//    var approveDate: String? = null,
//
//    var requestStatus: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var balanceDepositId: Int = 0
}

@Entity(tableName = "Users")
data class User(val name: String, val email: String) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}