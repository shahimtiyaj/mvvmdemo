package com.netizen.mvvmdemo.worker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.database.LocalDatabase
import com.netizen.mvvmdemo.repository.HomeRepository

class ScheduleWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    private val TAG: String = ScheduleWorker::class.java.simpleName

    override fun doWork(): Result {
        return try {
            // sendNotification("Title", "Details")

            val balanceDepositDAO = LocalDatabase.getInstance(applicationContext).balanceDAO()
            val homeRepository = HomeRepository(balanceDepositDAO)

            val token = inputData.getString("token")
            val page = inputData.getInt("page", 0)

            homeRepository.getBalanceDepositListData(token.toString(), page)

            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

    fun sendNotification(title: String, message: String) {
        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(applicationContext, "default")
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.mipmap.ic_launcher)

        notificationManager.notify(1, notification.build())
    }

}