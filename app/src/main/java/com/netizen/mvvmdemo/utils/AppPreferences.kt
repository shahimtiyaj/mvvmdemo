package com.netizen.mvvmdemo.utils

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(context: Context?) {

    private val PREFERENCE_NAME = "app_preferences"
    private val KEY_TOKEN = "access_token"

    private var mContext: Context? = null

    private var mPreferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    init {
        mContext = context
        mPreferences = mContext!!.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        editor = mPreferences!!.edit()
    }

    fun setToken(token: String?) {
        editor!!.putString(KEY_TOKEN, token)
        editor!!.apply()
    }

    fun getToken(): String? {
        var token: String? = null
        if (mPreferences!!.contains(KEY_TOKEN)) {
            token = mPreferences!!.getString(KEY_TOKEN, "")
        }
        return token
    }
}