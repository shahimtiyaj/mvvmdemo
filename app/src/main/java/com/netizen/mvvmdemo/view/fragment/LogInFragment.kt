package com.netizen.mvvmdemo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.databinding.FragmentLogInBinding
import com.netizen.mvvmdemo.utils.AppPreferences
import com.netizen.mvvmdemo.utils.CustomProgressDialog
import com.netizen.mvvmdemo.utils.UserSession
import com.netizen.mvvmdemo.view.activity.MainActivity
import com.netizen.mvvmdemo.viewModel.LoginViewModel

class LogInFragment : Fragment() {

    private var loginViewModel: LoginViewModel? = null
    private var customProgressDialog: CustomProgressDialog? = null
    private lateinit var binding: FragmentLogInBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (UserSession(context).isUserLoggedIn()) {
            findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
        }

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_log_in, container, false)
        binding.lifecycleOwner = this

        // Hiding the action bar
        MainActivity.hideActionBar()

        initObservables()

        return binding!!.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProviders
            .of(this, LoginViewModel.LogInViewModelFactory())
            .get(LoginViewModel::class.java)
    }

     private fun initObservables() {
         if (customProgressDialog == null) {
             customProgressDialog = CustomProgressDialog(context)
         }

         binding.signin?.setOnClickListener {
             loginViewModel?.onLogInClick(binding?.userName?.text.toString(), binding?.password?.text.toString())
         }

         loginViewModel?.progressDialog?.observe(this, Observer {
             if (it) {
                 customProgressDialog = CustomProgressDialog(context)
                 customProgressDialog?.show()
             } else customProgressDialog?.dismiss()
        })

         loginViewModel?.isUserNameEmpty?.observe(this, Observer {
             if (it) {
                 binding.userName?.error = "Required Username"
                 binding.userName?.requestFocus()
             }
         })

         loginViewModel?.isPasswordEmpty?.observe(this, Observer {
             if (it) {
                 binding.password?.error = "Required Password"
                 binding.password?.requestFocus()
             }
         })

         loginViewModel?.accessToken?.observe(this, Observer {token ->
             if (token != null) {
                 AppPreferences(context).setToken(token)
             }
         })

         loginViewModel?.isLoggedIn?.observe(this, Observer {
             if (it) {
                 val userSession = UserSession(context)
                 userSession.createUserLogInSession()
                 userSession.setUserCredentials(binding!!.userName.text.toString())

                 loginViewModel?.isUserNameEmpty?.value = false
                 loginViewModel?.isPasswordEmpty?.value = false
//                 loginViewModel?.isLoggedIn?.value = false

                 binding.userName?.text = null
                 binding.password?.text = null

                 findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
             } else {
                 Snackbar.make(binding.userName, "Invalid user name or password", Snackbar.LENGTH_LONG).show()
             }
         })
    }
}
