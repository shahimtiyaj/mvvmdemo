package com.netizen.mvvmdemo.view.fragment

import android.app.Application
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.adapter.BalanceDepositListAdapter
import com.netizen.mvvmdemo.data.User
import com.netizen.mvvmdemo.database.LocalDatabase
import com.netizen.mvvmdemo.databinding.FragmentHomeDepositListBinding
import com.netizen.mvvmdemo.model.BalanceDepositGetData
import com.netizen.mvvmdemo.utils.AppPreferences
import com.netizen.mvvmdemo.utils.PaginationScrollListener
import com.netizen.mvvmdemo.utils.UserSession
import com.netizen.mvvmdemo.view.activity.MainActivity
import com.netizen.mvvmdemo.viewModel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home_deposit_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class HomeFragment : Fragment(), BalanceDepositListAdapter.OnItemClick {

    private lateinit var binding: FragmentHomeDepositListBinding
    private lateinit var homeViewModel: HomeViewModel
    private var recyclerView: RecyclerView? = null
    private var depositAdapter: BalanceDepositListAdapter? = null

    private var recyclerViewState: Parcelable? = null
    private lateinit var layoutManager: LinearLayoutManager

    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var page: Int? = 0

    private lateinit var preferences: AppPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_deposit_list, container, false)
        binding.lifecycleOwner = this

        MainActivity.setTitle("Home")
        MainActivity.showActionBar()

        recyclerView = binding.walletBalanceDepositLogList

        initObservables()

        val userList: List<User> = listOf(User("Tusar", "a"),
            User("Faisal", "b"))

        val userDao = LocalDatabase.getInstance(context!!.applicationContext).userDao()
        GlobalScope.launch {
            userDao.insert(userList)
        }

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferences = AppPreferences(context)
        homeViewModel = ViewModelProviders
            .of(this, HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)

        homeViewModel.clearPreviousData()
        homeViewModel.refreshDepositList(preferences.getToken()!!, 0)
    }

    override fun onResume() {
        super.onResume()
        MainActivity.setTitle("Home")
        MainActivity.setDisplayHomeAsUpEnabled(false)
    }

    override fun goToSingle(balanceDepositGetData: BalanceDepositGetData) {
        recyclerViewState = layoutManager.onSaveInstanceState()
        val action = HomeFragmentDirections
            .actionHomeFragmentToSingleDataFragment(balanceDepositGetData)
        findNavController().navigate(action)
    }

    private fun initObservables() {
        layoutManager = LinearLayoutManager(context)

        homeViewModel.balanceDepositMutableLiveData?.observe(viewLifecycleOwner, Observer { balanceDepositList ->
            depositAdapter = BalanceDepositListAdapter(activity!!, balanceDepositList, this)
            recyclerView?.layoutManager = layoutManager
            layoutManager.onRestoreInstanceState(recyclerViewState)
            recyclerView?.adapter = depositAdapter
            total_deposit_balance_logs.text= balanceDepositList.size.toString()
            binding.progressBar.visibility = View.GONE
        })

        homeViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (!it) progress_bar.visibility = View.GONE
            isLoading = it
        })

        homeViewModel.isLastPage.observe(viewLifecycleOwner, Observer {
            if (it) binding.progressBar.visibility = View.GONE
            isLastPage = it
        })

        homeViewModel.isSessionExpired.observe(viewLifecycleOwner, Observer {
            if (it) {
                UserSession(context).logOutUser()
                findNavController().navigate(R.id.action_homeFragment_to_signInFragment)
            }
        })

        homeViewModel.workLiveData?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.e("WORK_INFO", it[0].outputData.toString())
            }
        })

        recyclerView?.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                recyclerViewState = layoutManager.onSaveInstanceState()
                binding.progressBar.visibility = View.VISIBLE
                isLoading = true
                page = page?.plus(1)
                homeViewModel.refreshDepositList(preferences.getToken()!!, page!!)
            }
        })
    }
}
