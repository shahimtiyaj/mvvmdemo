package com.netizen.mvvmdemo.view.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.utils.UserSession

class MainActivity : AppCompatActivity() {

    companion object {
        private lateinit var mainSupportActionBar: ActionBar

        fun showActionBar() {
            mainSupportActionBar.show()
        }

        fun hideActionBar() {
            mainSupportActionBar.hide()
        }

        fun setTitle(title: String) {
            mainSupportActionBar.title = title
        }

        fun setDisplayHomeAsUpEnabled(enable: Boolean) {
            mainSupportActionBar.setDisplayHomeAsUpEnabled(enable)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainSupportActionBar = this.supportActionBar!!
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> super.onBackPressed()
            R.id.menu_log_out -> {
                UserSession(this).logOutUser()
                findNavController(R.id.nav_host_fragment).navigate(R.id.action_homeFragment_to_signInFragment)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
