package com.netizen.mvvmdemo.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.netizen.mvvmdemo.R
import com.netizen.mvvmdemo.databinding.FragmentSingleDataBinding
import com.netizen.mvvmdemo.view.activity.MainActivity

/**
 * A simple [Fragment] subclass.
 */
class SingleDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentSingleDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_single_data, container, false)

        MainActivity.setTitle("Single Data")
        MainActivity.setDisplayHomeAsUpEnabled(true)

        val args = SingleDataFragmentArgs.fromBundle(arguments!!)
        binding.textViewAccountNumber.text = args.balanceDeposit.transactionNumber

        return binding.root
    }
}
