package com.netizen.mvvmdemo.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//@JsonIgnoreProperties(ignoreUnknown = true)
@Parcelize
@Entity(tableName = "BalanceDeposit", primaryKeys = ["transactionNumber", "accountNumber"])
class BalanceDepositGetData : Parcelable {

//    @PrimaryKey(autoGenerate = true)
//    var balanceDepositId: Int = 0

    @SerializedName("requestDate")
    var requestDate: String? = null

    @SerializedName("bank")
    var bank: String? = null

    @SerializedName("fromWhere")
    var fromWhere: String? = null

    @NonNull
    @SerializedName("accountNumber")
    var accountNumber: String? = null

    @SerializedName("requestedAmount")
    var requestedAmount: Double? = null

    @SerializedName("transactionDate")
    var transactionDate: String? = null

    @NonNull
    @SerializedName("transactionNumber")
    var transactionNumber: String? = null

    @SerializedName("approveDate")
    var approveDate: String? = null

    @SerializedName("requestStatus")
    var requestStatus: String? = null

//    private var balanceDeposit: List<BalanceDepositGetData>? = null
//    fun getBalanceDepositList(): List<BalanceDepositGetData>? {
//        return balanceDeposit
//    }

//    constructor() {
//    }

//    constructor(
//        requestDate: String?,
//        bank: String?,
//        accountNumber: String?,
//        requestedAmount: Double?,
//        transactionDate: String?,
//        transactionNumber: String?,
//        approveDate: String?,
//        requestStatus: String?
//    ) {
//        this.requestDate = requestDate
//        this.bank = bank
//        this.accountNumber = accountNumber
//        this.requestedAmount = requestedAmount
//        this.transactionDate = transactionDate
//        this.transactionNumber = transactionNumber
//        this.approveDate = approveDate
//        this.requestStatus = requestStatus
//    }


}

