package com.netizen.mvvmdemo.model

import com.google.gson.annotations.SerializedName

class PostResponse {
    @SerializedName("access_token")
    var access_token: String? = ""

    fun getToken(): String? {
        return access_token
    }

    fun setToken(access_token: String?) {
        this.access_token = access_token
    }
}