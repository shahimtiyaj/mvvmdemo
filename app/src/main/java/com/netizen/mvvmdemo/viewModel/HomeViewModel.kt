package com.netizen.mvvmdemo.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.netizen.mvvmdemo.database.LocalDatabase
import com.netizen.mvvmdemo.repository.HomeRepository
import com.netizen.mvvmdemo.worker.ScheduleWorker
import java.util.concurrent.TimeUnit

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val balanceDepositDAO = LocalDatabase.getInstance(getApplication()).balanceDAO()

    private val homeRepository = HomeRepository(balanceDepositDAO)
    var balanceDepositMutableLiveData = homeRepository.balanceDepositMutableLiveData
    val isLoading = homeRepository.isLoading
    val isLastPage = homeRepository.isLastPage
    val isSessionExpired = homeRepository.isSessionExpired

    var workLiveData: LiveData<List<WorkInfo>>? = null

    fun refreshDepositList(token: String, page: Int) {
        homeRepository.getBalanceDepositListData(token, page)
    }

    fun clearPreviousData() {
        homeRepository.clearAll()
    }

    fun callPeriodicWorkRequest(token: String, page: Int) {
        val data = Data.Builder()
        data.putString("token", token)
        data.putInt("page", page)

        val constraints = Constraints.Builder()
            .setRequiresBatteryNotLow(true)
            .build()

        /**
         * One time work request
         */
//        val workRequest = OneTimeWorkRequest.Builder(ScheduleWorker::class.java)
//            .setInputData(data.build()).build()

        /**
         * Periodic work request
         */
        val workRequest = PeriodicWorkRequest.Builder(ScheduleWorker::class.java,
            PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS,
            TimeUnit.MILLISECONDS,
            PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS,
            TimeUnit.MILLISECONDS)
            .setConstraints(constraints)
            .addTag("worker")
            .setInputData(data.build())
            .build()

        /**
         * Getting the work manager
         */
        val workManager = WorkManager.getInstance(getApplication())

//        workManager.enqueue(workRequest)
        workManager.enqueueUniquePeriodicWork("Notification Checking",
            ExistingPeriodicWorkPolicy.KEEP,
            workRequest)

        /**
         * Cancelling works
         */
//        workManager.cancelWorkById(workRequest.id)
//        workManager.cancelAllWork()

        workLiveData = workManager.getWorkInfosByTagLiveData("worker")
    }

    class HomeViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                return HomeViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}