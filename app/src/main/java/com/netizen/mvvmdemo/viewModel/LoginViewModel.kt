package com.netizen.mvvmdemo.viewModel

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.mvvmdemo.model.LoginUser
import com.netizen.mvvmdemo.repository.LogInRepository

class LoginViewModel : ViewModel() {

    private val logInRepository = LogInRepository()

    val isUserNameEmpty = MutableLiveData<Boolean>()
    val isPasswordEmpty = MutableLiveData<Boolean>()

    val progressDialog = logInRepository.progressDialog
    val isLoggedIn = logInRepository.isLoggedIn
    var accessToken = logInRepository.accessToken

    init {
        isUserNameEmpty.value = false
        isPasswordEmpty.value = false
//        isLoggedIn.value = false
    }

    fun onLogInClick(userName: String, password: String) {
        when {
            TextUtils.isEmpty(userName) -> {
                isUserNameEmpty.value = true
            }
            TextUtils.isEmpty(password) -> {
                isPasswordEmpty.value = true
            }
            else -> {
                val loginUser = LoginUser(userName, password)
                logInRepository.submitData(loginUser)
            }
        }
    }

    class LogInViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                return LoginViewModel() as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }

    }
}