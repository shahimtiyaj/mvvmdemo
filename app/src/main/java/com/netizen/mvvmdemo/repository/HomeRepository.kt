package com.netizen.mvvmdemo.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonParser
import com.netizen.mvvmdemo.database.BalanceDepositDAO
import com.netizen.mvvmdemo.model.BalanceDepositGetData
import com.netizen.mvvmdemo.model.BalanceDepositPostData
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeRepository(private val balanceDepositDAO: BalanceDepositDAO) {

    val balanceDepositMutableLiveData: LiveData<List<BalanceDepositGetData>>? = balanceDepositDAO.getAllBalanceDepositList()
    val isLoading = MutableLiveData<Boolean>()
    val isLastPage = MutableLiveData<Boolean>()
    val isSessionExpired = MutableLiveData<Boolean>()

    fun getBalanceDepositListData(token: String, page: Int) {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer $token"
        header["Content-Type"] = "application/json"

        val balanceDeposit = BalanceDepositPostData("2019-01-01", "2019-12-23",
            "Deposit", "", 5, page)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.balanceDepositGetData(balanceDeposit, header)

        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<List<BalanceDepositGetData>> {
            override fun onResponse(call: Call<List<BalanceDepositGetData>>,
                                    response: Response<List<BalanceDepositGetData>>) {
                try {
                    when(response.code()) {
                        200 -> {
                            Log.e("onResponse", "Token:" + response.body())

                            isSessionExpired.value = false
                            val balanceDepositList = response.body() as List<BalanceDepositGetData>

                            if (balanceDepositList.isNotEmpty()) {
                                isLoading.value = false
                                saveBalanceDeposit(balanceDepositList)
                            } else {
                                isLastPage.value = true
                            }
                        }
                        401 -> {
                            val errorObject = response.errorBody()?.string()
                            val parseError = JsonParser().parse(errorObject)
                            val errorString = parseError.asJsonObject.get("error").asString

                            Log.e("RESPONSE", errorObject.toString())
                            Log.e("RESPONSE", errorString)

                            if (errorString.equals("invalid_token", true)) {
                                isSessionExpired.value = true
                            }
                        }
                    }
                } catch (e: Exception) {
                    Log.e("EXCEPTION", e.toString())
                }
            }

            override fun onFailure(call: Call<List<BalanceDepositGetData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    private fun saveBalanceDeposit(balanceDepositList: List<BalanceDepositGetData>?) {
        val balanceDepositGetData = balanceDepositList?.get(0)

        GlobalScope.launch {
            try {
                balanceDepositList?.let { balanceDepositDAO.insertBalanceDepositList(it) }
//            balanceDepositDAO.insertBalanceDeposit(balanceDepositGetData!!)
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    isLoading.value = false
                }
                Log.e("EXCEPTION", e.toString())
            }
        }
    }

    fun clearAll() {
        GlobalScope.launch {
            balanceDepositDAO.deleteAll()
        }
    }
}