package com.netizen.mvvmdemo.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.mvvmdemo.model.LoginUser
import com.netizen.mvvmdemo.model.PostResponse
import com.netizen.netiworld.rest.ApiClient
import com.netizen.netiworld.rest.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LogInRepository {

    val progressDialog = MutableLiveData<Boolean>()
    val isLoggedIn = MutableLiveData<Boolean>()
    var accessToken = MutableLiveData<String>()

    fun submitData(loginUser: LoginUser) {
        progressDialog.value = true

        val params = HashMap<String?, String?>()
        params["username"] = "test"
        params["password"] = "test"
        params["grant_type"] = "password"

        val header = HashMap<String?, String?>()
        header["Authorization"] = "Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ="
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["NZUSER"] = "${loginUser.getStrUser()}:${loginUser.getStrPassword()}:password"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postData(params, header)

        //calling the api
        call?.enqueue(object : Callback<PostResponse> {
            override fun onResponse(call: Call<PostResponse>, response: Response<PostResponse>) {
                Log.d("onResponse", "Access Token:" + response.body()?.getToken())

                if (response.code() == 200) {
                    isLoggedIn.value = true
                    accessToken.value = response.body()?.getToken().toString()
                } else {
                    isLoggedIn.value = false
                }

                progressDialog.value = false
            }

            override fun onFailure(call: Call<PostResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
                progressDialog.value = false
                Log.e("onFailure: ", progressDialog.value.toString())
            }
        })
    }
}